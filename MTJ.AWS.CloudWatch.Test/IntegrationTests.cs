using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace MTJ.AWS.CloudWatch.Test
{
    [TestClass]
    public class IntegrationTests
    {
        private static AwsCloudWatchLogger Logger;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            var client = new AwsCloudWatchClient();
            Logger = client.ConstructLogger("stream-swaps-api", "errors");
        }

        [TestMethod]
        public async Task LogImmediate()
        {
            await Logger.LogImmediately("TEST");
        }

        [TestMethod]
        public async Task Log()
        {
            for (int i = 0; i < 10; i++)
            {
                await Logger.Log((i + 1).ToString());
            }
        }
    }
}
