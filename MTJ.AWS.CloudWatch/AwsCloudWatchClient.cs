﻿using Amazon.CloudWatchLogs;
using Amazon.CloudWatchLogs.Model;
using MTJ.AWS.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MTJ.AWS.CloudWatch
{
    public class AwsCloudWatchClient : AwsClientBase<IAmazonCloudWatchLogs>
    {
        public AwsCloudWatchLogger ConstructLogger(string logGroupName, string logStreamName)
        {
            return new AwsCloudWatchLogger(logGroupName, logStreamName, ServiceClient);
        }
    }
}
