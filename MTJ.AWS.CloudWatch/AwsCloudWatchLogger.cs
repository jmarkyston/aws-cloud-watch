﻿using Amazon.CloudWatchLogs;
using Amazon.CloudWatchLogs.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MTJ.AWS.CloudWatch
{
    public class AwsCloudWatchLogger
    {
        private string LogGroupName;
        private string LogStreamName;
        private string SequenceToken;
        private IAmazonCloudWatchLogs ServiceClient;
        private List<InputLogEvent> Queue;

        public AwsCloudWatchLogger(string logGroupName, string logStreamName, IAmazonCloudWatchLogs serviceClient)
        {
            Queue = new List<InputLogEvent>();
            LogGroupName = logGroupName;
            LogStreamName = logStreamName;
            ServiceClient = serviceClient;
        }

        private InputLogEvent ConstructEvent(string message)
        {
            return new InputLogEvent()
            {
                Message = message,
                Timestamp = DateTime.Now
            };
        }

        private async Task Put(IEnumerable<InputLogEvent> inputLogEvents)
        {
            var request = new PutLogEventsRequest(LogGroupName, LogStreamName, inputLogEvents.ToList());
            request.SequenceToken = SequenceToken;
            try { await ServiceClient.PutLogEventsAsync(request); }
            catch (Exception x)
            {
                if (x is InvalidSequenceTokenException)
                {
                    SequenceToken = x.Message.Substring(x.Message.IndexOf(": ") + 2);
                    await Put(inputLogEvents);
                }
                else
                    throw x;
            }
        }

        public async Task Log(string message)
        {
            Queue.Add(ConstructEvent(message));
            if (Queue.Count >= 10)
            {
                await Put(Queue);
                Queue.Clear();
            }
        }

        public async Task LogImmediately(string message)
        {
            InputLogEvent logEvent = ConstructEvent(message);
            await Put(new InputLogEvent[] { logEvent });
        }
    }
}
